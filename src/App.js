import React from "react";
import styled from "styled-components";

import customers from "./data";
import MorphChart from "./MorphChart";

const Wrapper = styled.div`
  display: flex;

  .Sidebar {
    background-color: #e5e5e5;
  }

  h1 {
    color: #f05122;
  }
`;

function App() {
  return (
    <Wrapper>
      <div className="Sidebar">
        <h1>Morph Dataviz</h1>
        <h2>Simulated Customer Data</h2>
        <pre>{JSON.stringify(customers, undefined, 2)}</pre>
      </div>
      <div>
        <MorphChart data={customers} />
      </div>
    </Wrapper>
  );
}

export default App;
