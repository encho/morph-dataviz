import React, { useState, useEffect } from "react";
import { Spring, useTransition, animated } from "react-spring";
import { scaleLinear } from "d3-scale";
import { extent } from "d3-array";

// TODO integrate flubber interpolation example
// https://codesandbox.io/embed/lwpkp46om

const MorphChart = ({ data }) => {
  const amountExtent = extent(data, d => d.amount);
  const ageExtent = extent(data, d => d.age);

  const amountScale = scaleLinear()
    .domain(amountExtent)
    .range([0, 400]);

  const ageScale = scaleLinear()
    .domain(ageExtent)
    .range([0, 400]);

  const transitions = useTransition(
    data.map(item => ({
      ...item,
      y: amountScale(item.amount),
      x: ageScale(item.age)
      // path:
    })),
    item => `${item.firstName}-${item.lastName}`, // TODO use id
    {
      from: { y: 0, opacity: 0 },
      leave: { y: 0, opacity: 0 },
      enter: ({ y, x }) => ({ y, x, opacity: 1 }),
      update: ({ y, x }) => ({ y, x })
    }
  );

  return (
    <div>
      <h2>chart!</h2>
      <div style={{ display: "flex", position: "relative" }}>
        {transitions.map(({ item, props: { x, y, opacity }, key }, i) => (
          <animated.div
            key={key}
            style={{
              position: "absolute",
              zIndex: data.length - i,
              width: "10px",
              height: "10px",
              top: y,
              left: x,
              opacity,
              padding: "5px",
              backgroundColor: "yellow"
            }}
          ></animated.div>
        ))}
      </div>
    </div>
  );
};

export default MorphChart;
