import faker from "faker";
import { range } from "lodash";
import { differenceInSeconds } from "date-fns";

const now = new Date();

const makeCustomer = () => {
  return {
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    amount: parseFloat(faker.finance.amount()),
    birthday: new Date(faker.date.past())
  };
};

const addAge = customer => ({
  ...customer,
  age: differenceInSeconds(now, customer.birthday) / (60 * 60 * 24 * 365)
});

const customers = range(10)
  .map(makeCustomer)
  .map(addAge);

export default customers;
